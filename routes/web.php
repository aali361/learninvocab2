<?php
use App\User;
use App\Word;
use App\UserWord;
use App\People;
use App\WordGroup;
use Illuminate\Http\Request;
use App\Exam;


use Classes\Command\Invoker;

// use Log;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {return view('welcome');});

Route::post('vocabbot.php','botController@control');


Route::get('checkQueuedCommands','botController@checkQueuedCommands');

Route::get('/getWordsFromWeb',function()
{
    $groups=['GRE', 'TOEFL', 'IELTS'];
    
    ini_set('max_execution_time', 9999);
    ini_set('max_input_time ', 9999);
    $failCount=0;
    $pyPath = 'python3.6';
	$config='2>&1'; //Redirect stderr to stdout, so you can see errors
    
    foreach ($groups as $key=>$group)
    {
        $appPath=app_path().'\/PythonApps\/'.$group.'.py';
        $command="$pyPath $appPath $config";
        exec($command, $words, $status);
        Log::info(count($words));
        foreach ($words as $key =>$word ) 
        {
            WordGroup::create(['word'=>$word,'group'=>$group]);
        }
    }
}); 



Route::get('/getGRE',function()
{
    $vocabs=WordGroup::where('group','IELTS')->get();
    foreach ($vocabs as $vocab) {
        echo($vocab->word."<br>");
        # code...
    }
    // dd(WordGroup::where('group','GRE')->get());
});

Route::view('/','login');

Route::post('login',function(Request $request){
    if($request->userName=="admin" && $request->passWord=="admin")
    {
        Log::info("User Logged In to Web");
        return view("panel");
    }
});


Route::post('newExam',function(Request $request){
    return "Hello";
});

Route::post('getUsersList',function(){
    $users=User::all('fname', 'lname', 'username', 'disabled', 'userId', 'score');
    foreach ($users as $user)
    {
        if($user->disabled)        
        {
            $user->disabled="غیرفعال";
            $user->action='<button class="active-btn btn btn-sm btn-success" onclick="activeUser('.$user->userId.')">فعال کردن</button>';
        }else{
            $user->disabled="فعال";
            $user->action='<button class="deactive-btn btn btn-sm btn-danger" onclick="deActiveUser('.$user->userId.')">غیر فعال کردن</button>';
        }
    }
    Log::info($users);
    return response($users,200);
});


Route::post('activeUser/{id}',function($id)
{
    Log::info($id);
    $user=User::where('userId',(int)$id)->get()->first();
    $user->disabled=false;
    $user->save();
    return response("Ok",200);
});
Route::post('deActiveUser/{id}',function($id)
{
    Log::info($id);
    $user=User::where('userId',(int)$id)->get()->first();
    $user->disabled=true;
    $user->save();
    return response("Ok",200);

});

Route::post('newExam', 'examController@new');
Route::get('getExams',  'examController@index');
// Route::get('cre', 'examController@createExamQuestions');

Route::post('suspendExam', 'examController@suspendExam');
Route::get('checkExams', 'examController@checkMustHeldExams');

Route::get('checkExamples', function(){
    Log::info("Check Must Send Examples");
    $invoker = new Invoker();
    $invoker->run();
});


Route::get('findNewExamples', 'botController@findNewExamples');
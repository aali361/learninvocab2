<?php
namespace App;
use Log;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class ExamUser extends Eloquent 
{
    protected $fillable = ['_id','userId','examId'];
    protected $collection='ExamUser';
    protected $primaryKey='_id';
}
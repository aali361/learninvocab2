<?php
namespace App;
use Log;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Exam extends Eloquent 
{
    protected $fillable = ['_id', 'group', 'number', 'time', 'type', 'done', 'suspended'];
    protected $collection='Exam';
    protected $primaryKey='_id';
}
<?php

// Our ***Decorator*** pattern
// Our base class (actually interface) is Message.php class (in this same folder)
// Our core is TextMessage.php
// Our layers are InlineKeyboardDecorator and TextIndicator pattern

namespace Classes\Message;

use Log;
use Message;

class TextIndicatorDecorator extends Decorator
{
    public function setIndicator($type, $word="", $selectedItem="",$correctAnswer="")
    {
        $message=$this->textMessage->getMessage();
        $endMessage="";
        $startMessage="";
        switch ($type) 
        {
            case 'sDef':
                $startMessage="<b>Short Definition".$word.":</b> \n";
                break;
            case 'lDef':
                $startMessage="<b>Long Definition".$word.":</b> \n";
                break;
            case 'example':
                $startMessage="<b>Example:</b> \n";
                break;
            case 'notFound':
                $startMessage="🔴 <b>Not Found</b> 🔴\n";
                break;
            case 'confirmNewWord':
                $startMessage="✅<b>Ok</b>, ";
                break;
            case 'confirmLearnIt':
                $startMessage="✅";
                break;
            case 'newExample':
                $startMessage="📣 New Example for <b>".ucfirst($word)."</b> is found:\n\n";
                break;
            case 'reviewMessage':
                $startMessage="📖";
                break;
            case 'invalidInput':
                $startMessage="⛔️ ";
                break;
            case 'banned':
                $startMessage="⛔️⛔️⛔️ ";
                break;
            case 'examRow':
                $startMessage="⚪️ <b>Exam</b>".PHP_EOL.PHP_EOL;
                break;
            case 'correctAnswer':
                $endMessage="✅ Item ".$selectedItem." is <b>Correct Answer</b>".PHP_EOL.PHP_EOL."You got <b>+5 score</b>👏👏👏.";
                break;
            case 'wrongAnswer':
                $endMessage="❌❌❌ Item ".$selectedItem." is <b>Wrong Answer</b>. Correct one was ".$correctAnswer." item.".PHP_EOL.PHP_EOL."You got <b>-5 score</b> ⛔️⛔️⛔️.";
                break;
            case 'examEnrollConfirm':
                $endMessage=PHP_EOL.PHP_EOL."✅ <b>You enrolled to this exam!</b>";
                break;
            case 'examUnrollConfirm':
                $endMessage=PHP_EOL.PHP_EOL."❌ <b>You unrolled to this exam!</b>";
                break;
            case 'suspendExam':
                $startMessage="📢🔴 <b>Exam Cancellation</b>".PHP_EOL.PHP_EOL;
                $endMessage="Sorry for our changes. 🌹";
                break;
            case 'examIsSuspended':
                $endMessage=PHP_EOL."❌ Sorry, This Exam has <b>cancelled</b> by moderator team.";
                break;
            default:
                break;
        }
        $message['text']=$startMessage.$message['text'];
        $message['text']=$message['text'].$endMessage;
        $this->textMessage->setMessage($message);
    }
    public function getMessage()
    {
        // Log::info("getMessage of TextIndicatorDecorator");
        return $this->textMessage->getMessage();
    }
}
<?php

// Our ***Decorator*** pattern
// Our base class (actually interface) is Message.php class (in this same folder)
// Our layers are InlineKeyboardDecorator and TextIndicator pattern



namespace Classes\Message;
use Log;
class TextMessage implements Message
{
    public $message;
    public function __construct($text)
    {
        $message=array();
        $message['text']=$text;
        $this->message=$message;
    }
    public function getMessage()
    {
        // Log::info("getMessage of TextMessage Class");
        // log::info($this->message);
        return $this->message;
    }
    public function setMessage($message)
    {
        // log::info("setMessage of TextMessage Class");
        $this->message=$message;
        // log::info($this->message);
    }
}
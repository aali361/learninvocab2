<?php

// Our ***Decorator*** pattern
// Our base class (actually interface) is Message.php class (in this same folder)
// Our core is TextMessage.php
// Our layers are InlineKeyboardDecorator and TextIndicator pattern

namespace Classes\Message;

use Log;
abstract class Decorator implements Message
{
    public $textMessage;
    public function __construct(Message $textMessage)
    {
        // Log::info("Decorator constructor");
        $this->textMessage=$textMessage;
    }
}
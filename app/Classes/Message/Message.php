<?php


// Our ***Decorator*** pattern
// Our base class (actually interface) is Message.php class (in this same folder)
// Our core is TextMessage.php
// Our layers are InlineKeyboardDecorator and TextIndicator pattern

namespace Classes\Message;

interface Message
{
    public function getMessage();
}
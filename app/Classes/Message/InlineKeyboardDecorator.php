<?php

// Our ***Decorator*** pattern
// Our base class (actually interface) is Message.php class (in this same folder)
// Our core is TextMessage.php
// Our layers are InlineKeyboardDecorator and TextIndicator pattern


namespace Classes\Message;

use Log;
// use Message;

class InlineKeyboardDecorator extends Decorator
{
    public $buttons;
    public function setButtons($buttons)
    {
        
        // Log::info("Setting Buttons to a Message");
        $message=$this->textMessage->getMessage();
        $this->buttons=$buttons->getButtons();
        $message["reply_markup"]=json_encode(['inline_keyboard'=>$this->buttons]);
        $this->textMessage->setMessage($message);
    }
    public function getMessage()
    {
        // Log::info("getMessage of InlineKeyboardDecorator");
        return $this->textMessage->getMessage();
    }
}
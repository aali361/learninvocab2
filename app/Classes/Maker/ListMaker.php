<?php

// This class is related to our ***Factory Method*** pattern

namespace Classes\Maker;
use Log;
use App\Sentence;
use App\UserWord;
use App\Word;
use App\WordList;
use Classes\Message\TextIndicatorDecorator;
use Classes\Message\InlineKeyboardDecorator;
use Classes\Message\TextMessage;
use Classes\Maker\ButtonMaker;


// This class will create list of Sentences and Words (learned and unlearned)

class ListMaker
{
    // Whenever user search a word, we send 4 exampl to her
    // We use this function to create a list of this sentences
    // And then we send first sentence to user
    // First sentence has Next button, so that user can see other sentences
    public function examplesSentencesList($word,$examples)
    {
        log::info("Making a list of examples for user");
        $headSentence;
        $examples=json_decode($examples,true);
        $prevSentence=null;
        foreach ($examples as $key => $example)
        {
            $sentence=Sentence::create(['word'=>$word->word,'sentence'=>$example['sentence'],'link'=>$example['link'],'prev'=>null,'next'=>null]);
            if(isset($prevSentence))
            {
                $sentence->prev=$prevSentence['_id'];
                $prevSentence->next=$sentence['_id'];
                $sentence->save();
                $prevSentence->save();
            }
            $prevSentence=$sentence;
            if($key==0)
            {
                $headSentence=$sentence;
                $headSentence->volumeId=$example['volumeId'];
            }
        }
        if(!isset($headSentence))
            exit();
        $headSentenceMessage=new TextMessage($headSentence['sentence']."\n".$headSentence['link']);
        $exDecorator=new TextIndicatorDecorator($headSentenceMessage);
        $exDecorator->setIndicator('example');
        $butMaker=new ButtonMaker();
        $butMaker->addButton("Next Example ➡️","ex_".$headSentence['next']);
        $keyboardDecorator=new InlineKeyboardDecorator($headSentenceMessage);
        $keyboardDecorator->setButtons($butMaker);
        $headSentenceMessage->volumeId=$headSentence->volumeId;
        return $headSentenceMessage;
    }


    // Whenever user wants to review it's words, we create a list of that words
    // User can iterate over this list by Next and Prev buttons
    public function wordsReviewList($userId,$learned)
    {
        Log::info("We going to crate a list for review user words");
        $userWords=UserWord::where('userId',$userId)->where('learnStatus',$learned)->get();
        $message;
        $headOfList;
        $prevItem=null;
        foreach ($userWords as $key => $item)
        {
            log::info($item->wordId);
            $word=Word::find($item->wordId);
            $item=WordList::create(['word'=>$word->word,'sDef'=>$word->sDef,'number'=>($key+1)]);
            if(isset($prevItem))
            {
                $prevItem->update(['nextWord'=>$item->_id]);
            }
            $prevItem=$item;
            if($key==0)
            {
                $headOfList=$item;                
            }
            }
        $headMessage=new TextMessage($headOfList->number.". <b>".ucfirst($headOfList->word)."</b>".PHP_EOL.json_decode($headOfList->sDef));
        $btMaker=new ButtonMaker();
        if(isset($headOfList['nextWord']))
        {
            $btMaker->addButton("Next ➡️","nextWord_".$headOfList['_id']."_".$headOfList['nextWord']);
        }
        else
        {
            $btMaker->addButton("👏 Review Is Completed 👏","nothing_");
        }
        $keyboardDecorator=new InlineKeyboardDecorator($headMessage);
        $keyboardDecorator->setButtons($btMaker);
        return $headMessage;
    }
}
<?php

// This class is related to our ***Factory Method*** pattern

namespace Classes\Maker;
use Log;


// In Telegram buttons are a JSON object
// We use this class to hide shape of JSON in a main controller, So main controller will looks better
class ButtonMaker
{
    public $allButtons=array();
    public $currentRowButton=array();
    public function __construct()
    {
        // Log::info("ButtonMaker Constructor");
    }
    public function addButton($label,$value,$newRow=false)
    {
        if($newRow)
        {
            array_push($this->allButtons,$this->currentRowButton);    
            $this->currentRowButton=array();
        } 
        array_push($this->currentRowButton,['text'=>$label,'callback_data'=>$value]);

    }
    public function getButtons()
    {
        array_push($this->allButtons,$this->currentRowButton);
        return $this->allButtons;
    }
}
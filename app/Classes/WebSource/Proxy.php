<?php

// Our ***Proxy*** Pattern
// This class will reference to our main resource

namespace Classes\WebSource;

class Proxy extends WebSource
{
    public $remote;
    public function __construct(){
        $this->remote=new VocabularyCom();
    }
    public function getShortDef($word){
        return $this->remote->getShortDef($word);
    }
    public function getLongDef($word){
        return $this->remote->getLongDef($word);
    }
    public function getExamples($num,$word){
        return $this->remote->getExamples($num,$word);
    }
}
<?php

// Our ***Proxy Method*** 
// Each websource that we use in future must implement these functions


namespace Classes\WebSource;
abstract class WebSource
{
    public abstract function getShortDef($word);
    public abstract function getLongDef($word);
    public abstract function getExamples($num,$word);
}
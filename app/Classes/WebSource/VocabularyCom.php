<?php

// Our ***Proxy*** Pattern
// This class will get Short and Long definitions from the web
// This class also will get examples from web
// This class use www.vocabulary.com as our resource


namespace Classes\WebSource;
use App\Word;
use Log;
class VocabularyCom extends WebSource{

    //Our configuration for executing python file that fetch required information from web
    public $pyPath = 'python3.6';
    public $config = '2>&1';
    public $pythonAppsFolder='/PythonApps/';


    public $newVocab; //Our new Vocab that we gonna search for it's definitions and examples

    //We going to get short definition from vocabulary.com
    public function getShortDef($word){
        // Log::info("in getShortDef function in vocabularycom");
        // Log::info("Getting short def in vocabulary.com");
        $appPath=app_path().$this->pythonAppsFolder.'getVocabDef.py';
        $command="$this->pyPath $appPath $this->config $word";
        exec($command, $def, $status);
        $shortDef=$def[0];
        return $shortDef;

    }

    //We going to get long definition from vocabulary.com
    public function getLongDef($word){
        // Log::info("Getting long def in vocabulary.com");
        $appPath=app_path().$this->pythonAppsFolder.'getVocabDef.py';
        $command="$this->pyPath $appPath $this->config $word";
        exec($command, $def, $status);
        $longDef=$def[1];
        return $longDef;
    }

    //We going to get  $num examples from vocabulary.com
    public function getExamples($num,$word)
    {
        // Log::info("we going to fetch examples from our web source vocabulary.com");
        $appPath=app_path().$this->pythonAppsFolder.'getVocabExam.py';
        $command="$this->pyPath $appPath $this->config $word";
        exec($command, $examples, $status);
        $examples=$examples[0];
        $examples=json_decode($examples);
        try{
            $size=sizeof($examples->result->sentences);
        }catch(\Exception $e)
        {
            Log::alert('We get bad result for examples of '.$word);
            // $this->makeHTTPRequest('sendMessage',[
            //     'chat_id'=>$chatId,
            //     'text'=>"Something goes wrong, please try again!",
            //     'parse_mode'=>'html'
            // ]);
            exit();
        }
        $examples=$examples->result->sentences;
        $sentences=array();
        foreach ($examples as $key=>$example)
        {
            if($key==$num) break;
            $sentence=$example->sentence;
            $sentence=json_decode($sentence);
            if(isset($example->volume->locator))
            {
                $link=$example->volume->locator;
            }else{
                $link="<i>---Link isn't available!</i>";
            }
            array_push($sentences,array('number'=>++$key,'sentence'=>$sentence,'link'=>$link,'volumeId'=>$example->volumeId));
        }
        return json_encode($sentences);
    }
}
<?php
namespace Classes;
use Log;
use App\QueueCommand;
class TelegramCommand 
{

    // We use this function to execute http request to telegram server
    public function makeHTTPRequest($method,$datas=[])
    {
        if (!defined('API_KEY')) define('API_KEY', '636646290:AAE4m3Y5VWSNvDyVs7d60lp1QRpzt4V8WA0');
        $url = "https://api.telegram.org/bot".API_KEY."/".$method;
        $ch = curl_init();
        curl_setopt($ch,CURLOPT_URL,$url);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
        curl_setopt($ch,CURLOPT_POSTFIELDS,http_build_query($datas));
        $res = curl_exec($ch);
        if(curl_error($ch))
        {
            var_dump(curl_error($ch));
        }else
        {
            return json_decode($res);
        }
    }


    // By this function we send message to user
    public function sendMessage($user,$message)
    {
        // log::info("sendMessage function");
        $message=$message->getMessage();
        $message['chat_id']=$user;
        $message['parse_mode']='html';
        // log::info($message);
        $this->makeHTTPRequest('sendMessage',$message);
    }

    // By this function we edit message that we had sent to our user
    public function editMessage($user,$messageId,$message)
    {
        $message=$message->getMessage();
        $message['chat_id']=$user;
        $message['message_id']=$messageId;
        $message['parse_mode']='html';
        $this->makeHTTPRequest('editMessageText',$message);
    }

    // We respond to a callBack 
    public function callBackOk($callbackQueryId)
    {
        $this->makeHTTPRequest('answerCallbackQuery',['callback_query_id'=>$callbackQueryId]);
    }

    // Execute Queued Command
    public function executeQueuedCommand($userId,$message)
    {
        log::info("executeQueuedCommand");
        $message['chat_id']=$userId;
        $message['parse_mode']='html';
        // log::info($message);
        $this->makeHTTPRequest('sendMessage',$message);
    }



}
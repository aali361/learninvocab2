<?php

// This is our Invoker (command pattern)
// This class checks for commands that need to be executed

namespace Classes\Command;
use Log;
use App\Sentence;
use App\Word;
use App\UserWord;
use App\WordList;
use App\Exam;
use App\User;
use App\QueueCommand;
use App\Question;



use Classes\TelegramCommand;
use Classes\Message\TextIndicatorDecorator;
use Classes\Message\InlineKeyboardDecorator;
use Classes\Message\TextMessage;
use Classes\Maker\ButtonMaker;
use Classes\Maker\ListMaker;
use Classes\Exam\ExamUserManager;
use Classes\Exam\SimpleNotifier;

class Invoker
{

    // This function check if there is any command (message) that need to be executed
    public function run()
    {
        // log::info("Check Queued Commands (actually messages)");

        // Getting queued commands from database
        $todayCommands=QueueCommand::where('executeDate',date('Y-m-d'))->get();
        // log::info($todayCommands);
        $command=new TelegramCommand();
        date_default_timezone_set('Asia/Tehran');
        $currentHour=date('H');
        foreach ($todayCommands as $key => $queuedCommand)
        {
            if(true||$currentHour>=$queuedCommand->executeTime)
            {
                
                $command->executeQueuedCommand($queuedCommand->userId,$queuedCommand->message);
                $queuedCommand->delete();
            }
        }
    }
    public function add($userId,$message,$executeDate,$executeTime)
    {
        // This function adds a command (message) to our queued command table 
        // Each command (message) has execute time and execute date
        Log::info("Add Function of Invoker Class");
        QueueCommand::create(['userId'=>$userId,'message'=>$message->getMessage(),'executeDate'=>$executeDate,'executeTime'=>$executeTime]);
    }
}
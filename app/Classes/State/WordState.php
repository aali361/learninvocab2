<?php

// Our *** State Pattern ***

// Users have two state
// A normal state (word) where we want user to send word to us
// A number state when we want a number from user to specify 
// maximum number of examples that each time
// we send to use


namespace Classes\State;
use Log;
use Classes\Message\TextMessage;
use Classes\TelegramCommand;
use Classes\Message\TextIndicatorDecorator;

class WordState implements State
{
    public $user;
    public function __construct($user)
    {
        $this->user=$user;
    }

    // Message must be single word 
    public function validateInput($message)
    {
        log::info("validateInput function from WordState Class");
        $words=explode(" ",$message);
        if(1 === preg_match('~[0-9]~', $message)||count($words)>1)
        {
            log::info("Invalid Input");
            $this->validationFailed();
            exit();
        }
    }
    // If the message failed validation we send appropriate message
    public function validationFailed()
    {
        log::info("validate Failed");
        $failedMessage=new TextMessage("We expect just <b>one word</b>, Plase check your word!");
        $failedMessageDecorator=new TextIndicatorDecorator($failedMessage);
        $failedMessageDecorator->setIndicator('invalidInput');
        $command=new TelegramCommand();
        $command->sendMessage($this->user->userId,$failedMessage);
    }
}
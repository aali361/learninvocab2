<?php

// Our *** State Pattern ***

// Users have two state
// A normal state (word) where we want user to send word to us
// A number state when we want a number from user to specify 
// maximum number of examples that each time
// we send to use

namespace Classes\State;

// Each state must implement these two function
interface State
{
    public function validateInput($message);
    public function validationFailed();
}
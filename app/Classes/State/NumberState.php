<?php

// Our *** State Pattern ***

// Users have two state
// A normal state (word) where we want user to send word to us
// A number state when we want a number from user to specify 
// maximum number of examples that each time
// we send to use


namespace Classes\State;
use Log;
use Classes\Message\TextMessage;
use Classes\TelegramCommand;
use Classes\Message\TextIndicatorDecorator;

class NumberState implements State
{
    public $user;
    public function __construct($user)
    {
        $this->user=$user;
    }
    // Message must be number
    public function validateInput($message)
    {
        log::info("validateInput function from NumberState Class");
        if(!is_numeric($message))
        {
            log::info("validate Failed");
            $this->validationFailed();
            
        }
        $this->user->updateMaxExample($message);
        $this->user->changeState('word');
    }
    // If message was not number we will send appropriate message
    public function validationFailed()
    {
        $failedMessage=new TextMessage("We expect just <b>a number</b>, Plase check your input!");
        $failedMessageDecorator=new TextIndicatorDecorator($failedMessage);
        $failedMessageDecorator->setIndicator('invalidInput');
        $command=new TelegramCommand();
        $command->sendMessage($this->user->userId,$failedMessage);
        exit();

    }
}
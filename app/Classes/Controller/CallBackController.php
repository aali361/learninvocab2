<?php
namespace Classes\Controller;

use Log;
use Classes\TelegramCommand;
use App\Sentence;
use App\Word;
use App\UserWord;
use App\WordList;
use App\Exam;
use App\User;

use App\Question;


use Classes\Message\TextIndicatorDecorator;
use Classes\Message\InlineKeyboardDecorator;
use Classes\Message\TextMessage;
use Classes\Maker\ButtonMaker;
use Classes\Maker\ListMaker;
use Classes\Exam\ExamUserManager;
use Classes\Exam\SimpleNotifier;


// Each time that a user clicked on a button we have callback request
// We have create specific controller for callback requests
class CallBackController
{
    public $userId,$chatId,$messageId,$callBackData,$items;
    public $command;
    public function __construct($userId,$chatId,$messageId,$callbackQueryId,$callBackData)
    {
        Log::info("Callback data: ".$callBackData);
        $this->userId=$userId;$this->chatId=$chatId;$this->messageId=$messageId;$this->callBackData=$callBackData;
        $this->command=new TelegramCommand();
        $this->command->callBackOk($callbackQueryId);//Remove circular shape in telegram app
        $this->items=explode("_",$callBackData);
    }

    public function response()
    {
        if($this->items[0]=='ex')//we have to show next or prev example to user
        {
            $sentence=Sentence::find($this->items[1]);
            $message=new TextMessage($sentence['sentence']."\n".$sentence['link']);
            $exDecorator=new TextIndicatorDecorator($message);
            $exDecorator->setIndicator('example');
            $butMaker=new ButtonMaker();
            if(isset($sentence['prev']))
            {
                $butMaker->addButton("⬅️ Prev Example","ex_".$sentence['prev']);
            }
            if(isset($sentence['next']))
            {
                $butMaker->addButton("Next Example ➡️","ex_".$sentence['next']);
            }
            $keyboardDecorator=new InlineKeyboardDecorator($message);
            $keyboardDecorator->setButtons($butMaker);
            $this->command->editMessage($this->userId,$this->messageId,$message);
        }
        elseif($this->items[0]=='sd') // We going to show short definition to user
        {
            log::info("Short Def callback");
            $word=json_decode(Word::where('word',$this->items[1])->first());
            $message=new TextMessage($word->sDef);
            $shortDefDecorator=new TextIndicatorDecorator($message);
            $shortDefDecorator->setIndicator('sDef'," of ".ucfirst($this->items[1]));
            $butMaker=new ButtonMaker();
            $butMaker->addButton("⬅️ Back","ba_".$this->items[1]."_".$this->items[2]);
            $keyboardDecorator=new InlineKeyboardDecorator($message);
            $keyboardDecorator->setButtons($butMaker);
            $this->command->editMessage($this->userId,$this->messageId,$message);

        }
        elseif($this->items[0]=='ld') // We going to show long definition to user
        {
            log::info("Long Def callback");
            $word=json_decode(Word::where('word',$this->items[1])->first());
            $message=new TextMessage($word->sDef);
            $shortDefDecorator=new TextIndicatorDecorator($message);
            $shortDefDecorator->setIndicator('lDef'," of ".ucfirst($this->items[1]));
            $butMaker=new ButtonMaker();
            $butMaker->addButton("⬅️ Back","ba_".$this->items[1]."_".$this->items[2]);
            $keyboardDecorator=new InlineKeyboardDecorator($message);
            $keyboardDecorator->setButtons($butMaker);
            $this->command->editMessage($this->userId,$this->messageId,$message);

        }
        elseif($this->items[0]=='ba') // Back to the Example
        {
            log::info("Back Callback");
            $sentence=json_decode(Sentence::find($this->items[2]));
            $newExampleMessage=new TextMessage($sentence->sentence."\n".$sentence->link);
            $newExampleDecorator=new TextIndicatorDecorator($newExampleMessage);
            $newExampleDecorator->setIndicator('newExample',$this->items[1]);
            $btMaker=new ButtonMaker();
            $btMaker->addButton("ShortDef","sd_".$this->items[1]."_".$sentence->_id);
            $btMaker->addButton("LongDef","ld_".$this->items[1]."_".$sentence->_id);
            $btMaker->addButton("Learned it","le_".$this->items[1],true);
            $keyboardDecorator=new InlineKeyboardDecorator($newExampleMessage);
            $keyboardDecorator->setButtons($btMaker);
            $this->command->editMessage($this->userId,$this->messageId,$newExampleMessage);
        }
        elseif($this->items[0]=='le') // User say that I learn this vocab, So will not send new examples of it to the user
        {
            log::info("Learn it Callback");
            $word=Word::where('word',$this->items[1])->first();
            $userWord=UserWord::updateOrCreate(['userId'=>$this->userId,'wordId'=>$word->_id],['learnStatus'=>true]);
            $confirmMessage=new TextMessage("Ok, We will not send new examples of <b>".ucfirst($word->word)."</b> to you again!");
            $newExampleDecorator=new TextIndicatorDecorator($confirmMessage);
            $newExampleDecorator->setIndicator('confirmLearnIt');
            $this->command->editMessage($this->userId,$this->messageId,$confirmMessage);
        }
        elseif($this->items[0]=='leWo') // User want to review learned words
        {
            log::info("Review Learned Words");
            $listMaker=new ListMaker();
            $message=$listMaker->wordsReviewList($this->userId,true);
            $this->command->editMessage($this->userId,$this->messageId,$message);

        }
        elseif($this->items[0]=='unLeWo') // User want to review unlearned words
        {
            log::info("Review UnLearned Words");
            $listMaker=new ListMaker();
            $message=$listMaker->wordsReviewList($this->userId,false);
            $this->command->editMessage($this->userId,$this->messageId,$message);
        }
        elseif($this->items[0]=='nextWord') // User want to see next Word
        {
            Log::info("Next word needed");
            $currentWord=WordList::find($this->items[1]);
            $currentMessage=new TextMessage($currentWord->number.". <b>".ucfirst($currentWord->word)."</b>".PHP_EOL.json_decode($currentWord->sDef));
            $this->command->editMessage($this->userId,$this->messageId,$currentMessage);
            $nextWord=WordList::find($this->items[2]);
            $nextMessage=new TextMessage($nextWord->number.". <b>".ucfirst($nextWord->word)."</b>".PHP_EOL.json_decode($nextWord->sDef));
            $btMaker=new ButtonMaker();
            if(isset($nextWord['nextWord']))
            {
                $btMaker->addButton("Next ➡️","nextWord_".$nextWord['_id']."_".$nextWord['nextWord']);
            }
            else
            {
                $btMaker->addButton("👏 Review Is Completed 👏","nothing_");
            }
            $keyboardDecorator=new InlineKeyboardDecorator($nextMessage);
            $keyboardDecorator->setButtons($btMaker);
            $this->command->sendMessage($this->userId,$nextMessage);
        }
        elseif($this->items[0]=='enroll')
        {
            // User has enrolled an exam
            // Using Mediator pattern

            Log::info($this->items[1]);
            $examController=SimpleNotifier::getInstance();
            $examController->register($this->userId, $this->items[1], $this->messageId);
        }
        elseif($this->items[0]=='unroll')
        {
            // User has unrolled and exam
            // Using Mediator pattern

            Log::info($this->items[1]);
            $examController=SimpleNotifier::getInstance();
            $examController->unRegister($this->userId, $this->items[1], $this->messageId);
        }
        elseif($this->items[0]=='ans') // User answered to a question
        {
            // In Exam, User clicked on a option
            $question=Question::find($this->items[1]);
            if($this->items[2]==$this->items[3])
            {
                // User had correct answer
                User::find($this->userId)->increaseScore();
                $message=new TextMessage($question["message"]["message"]["text"].PHP_EOL.PHP_EOL);
                $answerIndicator=new TextIndicatorDecorator($message);
                $answerIndicator->setIndicator('correctAnswer',"",$this->items[2]+1);
                $this->command->editMessage($this->userId, $this->messageId, $message);
            }
            else
            {
                // User had wrong answer
                User::find($this->userId)->decreaseScore();
                $message=new TextMessage($question["message"]["message"]["text"].PHP_EOL.PHP_EOL);
                $answerIndicator=new TextIndicatorDecorator($message);
                $answerIndicator->setIndicator('wrongAnswer', "", $this->items[2]+1, $this->items[3]+1);
                $this->command->editMessage($this->userId, $this->messageId, $message);
            }
        }
    }
}
<?php

namespace Classes\Exam;
use Log;

use App\WordGroup;
use App\ExamUser;
use App\Exam;
use Classes\TelegramCommand;

use Classes\WebSource\Proxy;

abstract class ExamBuilder
{
    public $numberOfQuestions, $groupOfWords, $vocabs=array(), $sentences=array(), $questions=array(), $examId;

    public function __construct($examId, $numberOfQuestions, $groupOfWords)
    {
        $this->examId=$examId;
        $this->numberOfQuestions=$numberOfQuestions;
        $this->groupOfWords=$groupOfWords;
    }

    // We get random vocabs from specified group
    public function getExamVocabs()
    {
        log::info("getExamVocabs function");
        $vocabs=WordGroup::where('group',$this->groupOfWords)->get();
        $vocabs=$vocabs->shuffle();
        $vocabs=$vocabs->take($this->numberOfQuestions);
        foreach ($vocabs as $vocab)
        {
            array_push($this->vocabs,$vocab->word);
        }
    }

    // We get definitions for our randomly vocab
    public function getShortDefinitions()
    {
        log::info("get Definitions function");
        // Some vocabs has not definiton so are not worthy
        $worthyVocabs=array();
        $webSource=new Proxy();
        foreach ($this->vocabs as $vocab)
        {
            $shortDef = $webSource->getShortDef($vocab);
            if($shortDef=="0") continue;   
            $shortDef = json_decode($shortDef, true);
            array_push($this->sentences,$shortDef);
            array_push($worthyVocabs,$vocab);
        }
        $this->vocabs=$worthyVocabs;
    }

    // We have two subclass that implemented this function
    public abstract function makeQuestions();

    // At end, that our questions are ready, we send them to users
    public function sendQuestions()
    {
        log::info("sendQuestions");
        $command=new TelegramCommand();
        $usersOfThisExmam=ExamUser::where('examId', $this->examId)->get();
        foreach ($usersOfThisExmam as $key => $user)
        {
            foreach ($this->questions as $key => $question)
            {
                $command->sendMessage($user->userId, $question);
            }
        }
    }


    //At end we make that exam done
    public function makeDoneExam()
    {
        // Mark done exam
        Exam::find($this->examId)->update(['done'=>true]);
    }

    // Template Method
    public function buildExam()
    {
        $this->getExamVocabs();
        $this->getShortDefinitions();
        $this->makeQuestions(); //variant function
        $this->sendQuestions();
        $this->makeDoneExam();
    }
}
<?php

// This class is related to our ***Mediator*** Pattern


namespace Classes\Exam;
use Log;
use App\ExamUser;
use App\Exam;
use Classes\Message\TextMessage;
use Classes\Message\TextIndicatorDecorator;
use Classes\TelegramCommand;
use Carbon\Carbon;


// Our subclass of mediator pattern 
// This class will notify whenever an exam suspended
class SimpleNotifier extends ExamUserManager
{
    public function notify($examId)
    {
        // We want to inform users that we have suspended an exam
        Log::info("Notify Function");
        $usersOfExam=ExamUser::select('userId')->where('examId',$examId)->get();
        log::info($usersOfExam);
        $exam=Exam::find($examId);
        $exam->time=Carbon::createFromTimestamp($exam->time)->toDateTimeString();
        log::info($exam);
        $examSuspendationMessage=new TextMessage("We want to inform you that the exam that you was enrolled is cancelled.".PHP_EOL.PHP_EOL."Group: <b>".$exam->group.PHP_EOL."Time: ".$exam->time."</b>".PHP_EOL.PHP_EOL);
        $examSuspendationMessage=new TextIndicatorDecorator($examSuspendationMessage);
        $examSuspendationMessage->setIndicator('suspendExam');
        $command = new TelegramCommand();
        foreach ($usersOfExam as $key => $user)
        {
            $command->sendMessage($user->userId,$examSuspendationMessage);
        }
    }
}
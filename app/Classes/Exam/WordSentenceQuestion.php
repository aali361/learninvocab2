<?php

// This class is related to our ***Template*** pattern
// In this class we implement on of functions in Template method in base class
// In this class exams are created in way that question is a word and options are sentences


namespace Classes\Exam;
use Log;
use App\Question;
use App\WordGroup;
use Classes\WebSource\Proxy;


use Classes\Message\TextMessage;
use Classes\TelegramCommand;
use Classes\Maker\ButtonMaker;
use Classes\Message\InlineKeyboardDecorator;

class WordSentenceQuestion extends ExamBuilder
{
    public function makeQuestions()
    {
        log::info("makeQuestions functions");
        
        // In base calss we created some random vocabs that are placed in $this->vocabs
        foreach ($this->vocabs as $key=>$vocab)
        {
            $word=$vocab;
            $sentences=$this->sentences;
            $vocabs=$this->vocabs;
            $answerSentence=$sentences[$key];
            $answerVocab=$vocabs[$key]; 


            // For each vocab that we create a question we need 4 (or 3) vocab that create our options
            $randomVocabs=WordGroup::where('group',$this->groupOfWords)->get();
            $randomVocabs=$randomVocabs->shuffle();
            $randomVocabs=$randomVocabs->take(5); // We get 4 random vocabs but we just need 3 of them for making options of question
            // log::info($randomVocabs);
            $optionsSentence=array();
            $optionsVocab=array();
            
            // For each option we need short definition about that word
            $webSource=new Proxy();
            
            $count=0;
            foreach ($randomVocabs as $randomVocab)
            {
                if($count==4) break;
                if($vocab==$randomVocab->word) continue;
                $shortDef = $webSource->getShortDef($randomVocab->word);
                if($shortDef=="0") continue;   
                $shortDef = json_decode($shortDef, true);
                array_push($optionsSentence, $shortDef);
                array_push($optionsVocab, $randomVocab->word);
                $count++;
            }
            // After above foreach we has created our options


            $message="";
            // We put answer in a random position between 0 and 3 
            $answerPosition=mt_rand(0,3);

            // In this loop we have some definitions and we must blank it's related word. we do this in below loop
            foreach ($optionsSentence as $key1 => $option)
            {
                if($answerPosition==$key1)
                {
                    $sentence=preg_replace('/\b'.$answerVocab.'\b/i', " - - - - - - - - ", $answerSentence);  
                    $message=$message.PHP_EOL.PHP_EOL.($key1+1).". ".$sentence;
                    continue;
                }                
                $sentence=$option;
                $sentence=preg_replace('/\b'.$optionsVocab[$key1].'\b/i', " - - - - - - - - ", $sentence);
                $message=$message.PHP_EOL.PHP_EOL.($key1+1).". ".$sentence;
            }

            // Now we create message
            $message=new TextMessage("<b>Question ".($key+1).":</b>".PHP_EOL.PHP_EOL."Which Sentence Describe <b>".$vocab."</b>?".$message.PHP_EOL);
            $idOfQuestion=Question::create(['message'=>$message]);
            $btMaker=new ButtonMaker();
            $btMaker->addButton("1","ans_".$idOfQuestion->_id."_0_".$answerPosition);
            $btMaker->addButton("2","ans_".$idOfQuestion->_id."_1_".$answerPosition);
            $btMaker->addButton("3","ans_".$idOfQuestion->_id."_2_".$answerPosition);
            $btMaker->addButton("4","ans_".$idOfQuestion->_id."_3_".$answerPosition);
            $keyboardDecorator=new InlineKeyboardDecorator($message);
            $keyboardDecorator->setButtons($btMaker);


            // We put our questions to an array, so sendQuestions method in base class can access to them
            array_push($this->questions, $message);
            
        }
    }
}
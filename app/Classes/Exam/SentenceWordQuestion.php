<?php

// This class is related to our ***Template*** pattern
// In this class we implement on of functions in Template method in base class
// In this class exams are created in way that question is a word and options are sentences


namespace Classes\Exam;
use Log;
use App\Question;
use App\WordGroup;
use Classes\WebSource\Proxy;


use Classes\Message\TextMessage;
use Classes\TelegramCommand;
use Classes\Maker\ButtonMaker;
use Classes\Message\InlineKeyboardDecorator;

class SentenceWordQuestion extends ExamBuilder
{
    public function makeQuestions()
    {
        // Question will be a sentence
        // Options will be single word
        log::info("makeQuestions functions");
        $webSource=new Proxy();

        // In base calss we created some random vocabs that are placed in $this->vocabs
        foreach ($this->vocabs as $key=>$vocab)
        {
            $shortDef = $webSource->getShortDef($vocab);
            $shortDef = json_decode($shortDef);
            $shortDef=preg_replace('/\b'.$vocab.'\b/i', " - - - - - - - - ", $shortDef);
            $randomVocabs=WordGroup::where('group',$this->groupOfWords)->get();
            $randomVocabs=$randomVocabs->shuffle();
            $randomVocabs=$randomVocabs->take(5); // We get 4 random vocabs but we just need 3 of them for making options of question
            // log::info($randomVocabs);
            $optionsVocab=array();
            $count=0;

            // We check our random vocabs that are created in base class with options (must not be equal)
            foreach ($randomVocabs as $randomVocab)
            {
                if($count==4) break;
                if($vocab==$randomVocab->word) continue;
                array_push($optionsVocab, $randomVocab->word);
                $count++;
            }

            // After above foreach we has created our options

            $message="Which word is related to below definition?".PHP_EOL.PHP_EOL.$shortDef;
            $answerPosition=mt_rand(0,3);

            // We add our vocabs to out base message
            foreach ($optionsVocab as $key1 => $option)
            {
                if($answerPosition==$key1)
                {
                    $message=$message.PHP_EOL.PHP_EOL.($key1+1).". ".$vocab;
                    continue;
                }                
                $message=$message.PHP_EOL.PHP_EOL.($key1+1).". ".$option;
            }

            // Now we create message
            $message=new TextMessage("<b>Question ".($key+1).":</b>".PHP_EOL.PHP_EOL.$message.PHP_EOL);
            $idOfQuestion=Question::create(['message'=>$message]);
            $btMaker=new ButtonMaker();
            $btMaker->addButton("1","ans_".$idOfQuestion->_id."_0_".$answerPosition);
            $btMaker->addButton("2","ans_".$idOfQuestion->_id."_1_".$answerPosition);
            $btMaker->addButton("3","ans_".$idOfQuestion->_id."_2_".$answerPosition);
            $btMaker->addButton("4","ans_".$idOfQuestion->_id."_3_".$answerPosition);
            $keyboardDecorator=new InlineKeyboardDecorator($message);
            $keyboardDecorator->setButtons($btMaker);

            // We put our questions to an array, so sendQuestions method in base class can access to them
            array_push($this->questions, $message);
        }
    }
}
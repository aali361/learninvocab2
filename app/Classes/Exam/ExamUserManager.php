<?php

// This is our ***Mediator*** pattern 
// This class handle relationship between user and exam


namespace Classes\Exam;
use Log;

use App\ExamUser;
use App\Exam;
use Carbon\Carbon;
use Classes\Message\TextIndicatorDecorator;
use Classes\Message\TextMessage;
use Classes\TelegramCommand;

abstract class ExamUserManager
{
    // Singleton   Pattern
    protected static $instance = null;
    protected static $command = null;
    protected function __construct(){}
    protected function __clone(){}
    public static function getInstance()
    {
        if (!isset(static::$instance)) {
            static::$instance = new static;
            static::$command = new TelegramCommand();
        }
        return static::$instance;
    }

    // Our abstract function
    public abstract function notify($examId);


    // User enroll to an exam
    public function register($user, $exam, $messageId)
    {
        // User Register an exam
        log::info("Register function");
        ExamUser::firstOrCreate(['userId'=>$user,'examId'=>$exam]);
        $exam=Exam::find($exam);
        $exam->time=Carbon::createFromTimestamp($exam->time)->toDateTimeString();
        $message=new TextMessage("Group: <b>".$exam->group.PHP_EOL."</b>Date: <b>".$exam->time."</b>".PHP_EOL);
        $examIndicator=new TextIndicatorDecorator($message);
        $examIndicator->setIndicator('examRow');
        $examEndIndicator=new TextIndicatorDecorator($message);
        if($exam->suspended)
        {
            $examEndIndicator->setIndicator('examIsSuspended');
        }else{
            $examEndIndicator->setIndicator('examEnrollConfirm');
        }
        static::$command->editMessage($user, $messageId, $message);
    }
    // User unroll to an exam
    public function unregister($user, $exam, $messageId)
    {  
        log::info("UnRegister Function");
        $ExamUserRecord=ExamUser::where([['userId', $user],['examId', $exam]])->delete();
        $exam=Exam::find($exam);
        $exam->time=Carbon::createFromTimestamp($exam->time)->toDateTimeString();
        $message=new TextMessage("Group: ".$exam->group.PHP_EOL."Date: ".$exam->time.PHP_EOL);
        $examIndicator=new TextIndicatorDecorator($message);
        $examIndicator->setIndicator('examRow');
        $examEndIndicator=new TextIndicatorDecorator($message);
        $examEndIndicator->setIndicator('examUnrollConfirm');
        static::$command->editMessage($user, $messageId, $message);
    }
    
}
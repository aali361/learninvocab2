<?php
namespace App;
use Log;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class WordGroup extends Eloquent 
{
    protected $fillable = ['_id','word','group'];
    protected $collection='WordGroup';
    protected $primaryKey='_id';
}
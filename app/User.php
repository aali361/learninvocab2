<?php
namespace App;
use Log;
use Classes\State\NumberState;
use Classes\State\WordState;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

use Classes\Message\TextMessage;
use Classes\Message\InlineKeyboardDecorator;
use Classes\Message\TextIndicatorDecorator;
use Classes\TelegramCommand;

class User extends Eloquent 
{
    protected $fillable = ['_id', 'userId', 'fname', 'lname', 'username', 'state', 'disabled', 'score', 'maxExample', 'exampleSended'];
    protected $collection='User';
    protected $primaryKey='userId';
    public $word,$sDef,$lDef;

    public function  validateInput($messageText)
    {
        $valider="";
        switch ($this->state) 
        {
            case 'word':
                $valider=new WordState($this);
                break;
            case 'number':
                $valider=new NumberState($this);
                break;
                
            default:
                # code...
                break;
        }
        $valider->validateInput($messageText);
    }
    public function increaseScore()
    {
        $this->score=($this->score)+5;
        $this->save();
    }
    public function decreaseScore()
    {
        $this->score=($this->score)-5;
        $this->save();
    }
    public function changeState($state)
    {
        $this->state=$state;
        $this->save();
    }

    public function updateMaxExample($number)
    {
        log::info("updateMaxExample function");
        $command=new TelegramCommand();
        $getMaxExampleMessage=new TextMessage("Ok, We will send to you max <b>".$number."</b> example each time.");
        User::find($this->userId)->update(['maxExample'=>$number]);
        $command->sendMessage($this->userId, $getMaxExampleMessage);
        $this->state='word';
        $this->save();
        exit();
    }
    public function incrementExampleSended()
    {
        log::info("incrementExampleSended Function");  
        $this->exampleSended=$this->exampleSended + 1;
        $this->save();
    }
}
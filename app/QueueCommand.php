<?php
namespace App;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class QueueCommand extends Eloquent 
{
    protected $fillable = ['_id','userId','message','executeTime','executeDate'];
    protected $collection='QueueCommand';
    protected $primaryKey='_id';
}
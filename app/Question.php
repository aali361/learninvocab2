<?php
namespace App;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Question extends Eloquent 
{
    protected $fillable = ['_id','message'];
    protected $collection='Question';
    protected $primaryKey='_id';
}
<?php
namespace App;
use Log;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Word extends Eloquent 
{
    protected $fillable = ['_id','word','sDef','lDef'];
    protected $collection='Word';
    protected $primaryKey='_id';
}
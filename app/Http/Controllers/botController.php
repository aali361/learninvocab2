<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Log;
use App\Word;
use App\Sentence;
use App\User;
use App\Exam;
use App\UserWord;
use App\ExamUser;
use App\QueueCommand;
use DB;
use Carbon\Carbon;

use Classes\WebSource\Proxy;
use CLasses\Message\Message;
use Classes\Message\TextMessage;
use Classes\Message\InlineKeyboardDecorator;
use Classes\Message\TextIndicatorDecorator;
use Classes\TelegramCommand;
use Classes\Maker\ButtonMaker;
use Classes\Maker\ListMaker;
use Classes\Controller\CallBackController;
use Classes\Command\Invoker;


class botController extends Controller
{
    public $user,$chatId,$messageText,$messageId,$fname,$lname,$username,$command;

    public function control()
    {
        $update = json_decode(file_get_contents('php://input'));
        $this->setUpController($update);
        $this->identifyUser();
        $this->validateInput();


        $command=new TelegramCommand();

        //welcome message
        if($this->messageText=="/start")
        {
            $welcomeMessage=new TextMessage("Hello, Just send your word to me and I will learn that word to you!");
            $command->sendMessage($this->chatId,$welcomeMessage);
            exit();
        }
        if($this->messageText=="/update")
        {
            $this->findNewExamples();
            exit();
        }
        if($this->messageText=="/mywords")
        {
            $this->reviewWords();
            exit();
        }
        if($this->messageText=='/exams')
        {
            $this->exams();
            exit();
        }
        if($this->messageText=="/t")
        {
            Log::info("Testing QueueCommand");
            $invoker=New Invoker();
            $invoker->run();
            $queueCommand=QueueCommand::first();
            if(!isset($queueCommand))
                return;
            $userId=$queueCommand->userId;
            $message=$queueCommand->message;
            $command->executeQueuedCommand($userId,$message);
            $queueCommand->delete();
            exit();
        }
        if($this->messageText=="/setmaxexample")
        {
            $this->user->changeState('number');
            $this->getMaxExampleNumber();
            exit();
        }

        Log::info("Incoming message from user: ".$this->messageText);
        $word=Word::where('word',$this->messageText)->first();
        if(is_null($word))
        {
            Log::info("We have not found word in our database so we will try to fetch that from net");
            $webSource=new Proxy();
            $shortDef=$webSource->getShortDef($this->messageText);
            if($shortDef=='0')
            {
                Log::info("This word is not found in our web source");
                $notFoundMessage=new TextMessage("We have not found your word in our resources, Please check your word!");
                $notFoundDecorator=new TextIndicatorDecorator($notFoundMessage);
                $notFoundDecorator->setIndicator('notFound');
                $command->sendMessage($this->chatId,$notFoundMessage);
            }
            else
            {
                Log::info("This word is found in our web source and we will create new object of word");
                $longDef=$webSource->getLongDef($this->messageText);
                $word=Word::create(['word'=>$this->messageText,'sDef'=>$shortDef,'lDef'=>$longDef]);
                $word=Word::where('word',$this->messageText)->first();
                $this->sendWordInfo($word,$this->chatId,$command);
            }
        }
        else
        {
            Log::info("We have found this word in our database");
            $this->sendWordInfo($word,$this->chatId,$command);
        }
    }  






    public function reviewWords()
    {
        log::info("reviewWords Function");
        $userWordsLearned=UserWord::where('userId',$this->user->userId)->where('learnStatus',true)->get();
        $userWordsUnLearned=UserWord::where('userId',$this->user->userId)->where('learnStatus',false)->get();
        $reviewMessage=new TextMessage("Ok, There is <b>".$userWordsLearned->count()." Learned</b> and <b>".$userWordsUnLearned->count().
                                       " unLearned</b> words to review.".PHP_EOL."Which one do you want to review?");
        $reviewMessageDecorator=new TextIndicatorDecorator($reviewMessage);
        $reviewMessageDecorator->setIndicator('reviewMessage');
        $btMaker=new ButtonMaker();
        if($userWordsLearned->count()>0)
            $btMaker->addButton("Learned Words","leWo_".""."_"."");
        if($userWordsUnLearned->count()>0)
            $btMaker->addButton("UnLearned Words","unLeWo_".""."_"."");
        $keyboardDecorator=new InlineKeyboardDecorator($reviewMessage);
        $keyboardDecorator->setButtons($btMaker);
        $command=new TelegramCommand();
        $command->sendMessage($this->user->userId,$reviewMessage);
        exit();
    }


    public function setUpController($update)
    {
        $command=new TelegramCommand();
        if(isset($update->callback_query))
        {
            $this->chatId = $update->callback_query->message->chat->id;
            $this->userId = $update->callback_query->from->id;
            $this->messageId = $update->callback_query->message->message_id;
            $callbackQueryId=$update->callback_query->id;
            $callBackData=$update->callback_query->data;
            $this->identifyUser($command);
            $callBackController=new CallBackController($this->userId,$this->chatId,$this->messageId,$callbackQueryId,$callBackData);
            $callBackController->response();
            exit();
        }
        else
        {
            $this->chatId = $update->message->chat->id;
            $this->userId=$update->message->from->id;
            $this->fname=$update->message->from->first_name;
            $this->lname="No Set";
            $this->username="No Set";
            if(isset($update->message->from->username))
            {
                $this->username=$update->message->from->username;
            }
            if(isset($update->message->from->last_name))
            {
                $this->lname=$update->message->from->last_name;
            }
            // $this->user=User::find($this->userId);
            $this->messageText=strtolower($update->message->text);
        }
        
    }

    public function identifyUser()
    {
        $command=new TelegramCommand();
        $this->user=User::find($this->userId);
        if(isset($this->user))
        {
            log::info("Found user");
            if($this->user->disabled)
            {
                log::info("Found user but disabled");
                $confirmMessage=new TextMessage("Sorry you are banned from this bot!!!".PHP_EOL."Please Contact Admin.");
                $confirmMessageDecorator=new TextIndicatorDecorator($confirmMessage);
                $confirmMessageDecorator->setIndicator('banned');
                $command->sendMessage($this->userId,$confirmMessage);
                exit();
            }

        }else
        {
            log::info("Not found user so we will create one user");
            $this->user=User::create(['userId'=>$this->userId,'fname'=>$this->fname,'lname'=>$this->lname,'username'=>$this->username,'state'=>'word','disabled'=>false, 'score'=>0, 'exampleSended'=>0, ]);
        }
    }

    public function sendWordInfo($word,$chatId,$command)
    {
        $webSource=new Proxy();
        $word=json_decode($word);
        $shortDef=new TextMessage(json_decode($word->sDef));
        $longDef=new TextMessage(json_decode($word->lDef));
        $sDefDecorator=new TextIndicatorDecorator($shortDef);
        $sDefDecorator->setIndicator('sDef');
        $lDefDecorator=new TextIndicatorDecorator($longDef);
        $lDefDecorator->setIndicator('lDef');
        $command->sendMessage($chatId,$shortDef);
        $command->sendMessage($chatId,$longDef);
        $examples=$webSource->getExamples(4,$word->word);
        $exList=new ListMaker();
        $headSentence=$exList->examplesSentencesList($word,$examples);
        $userWord=UserWord::updateOrCreate(['userId'=>$chatId,'wordId'=>$word->_id],
                                           ['lastExample'=>$headSentence->volumeId,'learnStatus'=>false]);
        $command->sendMessage($chatId,$headSentence);
        $confirmMessage=new TextMessage("We will look into new examples for this word and we will send them to you once in while");
        $confirmMessageDecorator=new TextIndicatorDecorator($confirmMessage);
        $confirmMessageDecorator->setIndicator('confirmNewWord');
        $command->sendMessage($chatId,$confirmMessage);
    }

    public function findNewExamples()
    {
        log::info("find new examples function");
        $invoker= new Invoker();
        User::where('exampleSended'>0)->update(['exampleSended'=>0]);
        $command=new TelegramCommand();
        $userWordCollection=UserWord::where('learnStatus',false);
        if($userWordCollection->count()==0)
        {
            log::info("We don't have any unlearned word to find new examples about it");
            exit();
        } 
        $distinctWords=$userWordCollection->distinct('wordId')->get();
        foreach ($distinctWords as $key => $wordId)
        {
            $word=Word::find($wordId)->first();
            $webSource=new Proxy();
            $mostRecentExample=json_decode($webSource->getExamples(1,$word->word))[0];
            $wordId=json_decode($wordId)[0];
            $usersOfThisWord=UserWord::where('wordId',$wordId)->get();
            foreach ($usersOfThisWord as $key => $userOfThisWord) 
            {
                if($mostRecentExample->volumeId > $userOfThisWord->lastExample)
                {
                    Log::info("new example found");
                    $user=User::find($userOfThisWord->userId);
                    if($user->exampleSended==$user->maxExample)
                        continue;
                    $user->incrementExampleSended();
                    $sentence=Sentence::create(['word'=>$word->word,'sentence'=>$mostRecentExample->sentence,'link'=>$mostRecentExample->link]);
                    $newExampleMessage=new TextMessage($mostRecentExample->sentence."\n".$mostRecentExample->link);
                    $newExampleDecorator=new TextIndicatorDecorator($newExampleMessage);
                    $newExampleDecorator->setIndicator('newExample',$word->word);
                    $btMaker=new ButtonMaker();
                    // log::info($word->word);
                    $btMaker->addButton("ShortDef","sd_".$word->word."_".$sentence->_id);
                    $btMaker->addButton("LongDef","ld_".$word->word."_".$sentence->_id);
                    $btMaker->addButton("Learn it","le_".$word->word,true);
                    
                    $keyboardDecorator=new InlineKeyboardDecorator($newExampleMessage);
                    $keyboardDecorator->setButtons($btMaker);
                    // $command->sendMessage($userOfThisWord->userId,$newExampleMessage);
                    $invoker->add($userOfThisWord->userId,$newExampleMessage,date('Y-m-d'),'17');
                    $userWord=UserWord::where([['userId', $userOfThisWord->userId],['wordId',$word->_id]])->update(['lastExample'=>$mostRecentExample->volumeId]);
                }
                else{
                    // Log::info("we have not found new example of <".$word->word."> for this user(".$userOfThisWord->userId.") and this word");
                }
            }
        }
    }



    public function validateInput()
    {
        log::info("Going to validate user input");
        $this->user->validateInput($this->messageText);
    }

    public function checkArrivedExams()
    {
        log::info("Check Queued Commands");
        $exams=Exam::where('done',false)->get();
        foreach ($exams as $exam)
        {
            // log::info($exam);
            $date=$exam->date;
            log::info($date);
        }
    }

    public function exams()
    {
        // Log::info("User Wnats to See list of Exams");
        $command=new TelegramCommand();
        $exams=Exam::select('group', 'time')->where([['done', false],['suspended',false]])->get();
        if($exams->count()==0)
        {
            $message=new TextMessage("Currently There is no Exam Defined by Moderator.");
            $command->sendMessage($this->user->userId, $message);
            exit();
        }
        foreach ($exams as $exam) 
        {
            $exam->time=Carbon::createFromTimestamp($exam->time)->toDateTimeString();
            $message=new TextMessage("Group: <b>".$exam->group.PHP_EOL."</b>Time: <b>".$exam->time."</b>");
            $notFoundDecorator=new TextIndicatorDecorator($message);
            $notFoundDecorator->setIndicator('examRow');            
            $btMaker=new ButtonMaker();
            if(ExamUser::where([['userId',$this->user->userId],['examId',$exam->_id]])->get()->count()>0)
            {
                $btMaker->addButton("Unroll","unroll_".$exam->_id);
            }else{
                $btMaker->addButton("Enroll","enroll_".$exam->_id);
            }
            $keyboardDecorator=new InlineKeyboardDecorator($message);
            $keyboardDecorator->setButtons($btMaker);
            $command->sendMessage($this->user->userId, $message);

        }
    }





    public function getMaxExampleNumber()
    {
        $command=new TelegramCommand();
        log::info("getMaxExampleNumber Function");
        $getMaxExampleMessage=new TextMessage("Just send number of examples that each time you want get");
        $command->sendMessage($this->chatId,$getMaxExampleMessage);

    }

}

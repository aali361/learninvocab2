<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Log;
use App\Word;
use App\Sentence;
use App\User;
use App\UserWord;
use App\QueueCommand;
use Classes\Exam\WordSentenceQuestion;
use Classes\Exam\SentenceWordQuestion;
use App\Exam;
use App\ExamUser;
use DB;
use Carbon\Carbon;
use Classes\Exam\SimpleNotifier;

define("NUMBEROFQUESTIONS",5);


class examController extends Controller
{
    public function new(Request $request)
    {
        // Creating new Exam
        // Log::info("new exam creating");
        Log::info($request);
        if($request->group=="IELTH")
            $request->group=="GRE";
        log::info($request->dayOfExam);
        $date=new Carbon($request->dayOfExam." ".$request->hourOfExam);
        Exam::create(['type'=>$request->type, 'group'=>$request->group, 'time'=>$date->timestamp, 'done'=>false, 'suspended'=>false]);
        return response("Ok",200);
    }
    public function index()
    {
        // Getting list of exams
        Log::info("Getting list of exams");
        $exams=Exam::all();
        foreach ($exams as $exam )
        {
            log::info($exam->done);
            if($exam->done)
            {
                $exam->status='<span class="text-success" style="font-weight:bold;">انجام شده</span>';
                $exam->action='----';
            }
            else 
            {
                $exam->status='<span class="text-primary" style="font-weight:bold;">در انتظار برگزاری</span>';
                $exam->action='<button class="btn btn-sm btn-warning" onclick="examSuspend(\''.$exam->_id.'\')">معلق کردن</button>';
            }
            if($exam->suspended)
            {
                Log::info("Exam is suspended");
                $exam->status='<span class="bg-warning" >معلق شده</span>';
                $exam->action='';
            }
            $exam->time=Carbon::createFromTimestamp($exam->time)->toDateTimeString();
        }
        return response($exams,200);
    }

    public function heldExam($examId, $number, $group)
    {
        log::info("createExamQuestions");
        $exam=Exam::find($examId);
        $examBuilder;
        if($exam->type=="WtoS")
            $examBuilder=new WordSentenceQuestion($examId, $number, $group);
        else
            $examBuilder=new SentenceWordQuestion($examId, $number, $group);
        $examBuilder->buildExam();
    }

    public function suspendExam(Request $request)
    {
        // Admin has suspended an exam and we must inform to our 
        $examId=$request->id;
        $exam=Exam::find($examId);
        $exam->update(['suspended'=>true]);
        Log::info($examId);
        $examController=SimpleNotifier::getInstance();
        $examController->notify($examId);
    }

    public function checkMustHeldExams()
    {
        // Check Exams That must be Held
        log::info("Check Must Held Exams");
        $currentTime=time(); // Getting current time as timestamp
        $exams=Exam::where([['done',false],['suspended',false]])->get();
        foreach ($exams as $key => $exam)
        {
            // We check that if the exam time has arrived if so we will held it
            if($currentTime>=$exam->time)
            {
                Log::info($exam->_id." Must be created");
                if(ExamUser::where("examId",$exam->_id)->get()->count()>0)
                    $this->heldExam($exam->_id, NUMBEROFQUESTIONS, $exam->group); // If this Exam has any attendees we will build it
                else
                    Log::info("Exam has not any attendees"); 
            }
        }
    }
}

<?php
namespace App;
use Log;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class WordList extends Eloquent 
{
    protected $fillable = ['_id','number','word','sDef','nextWord'];
    protected $collection='WordList';
    protected $primaryKey='_id';
}
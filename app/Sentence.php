<?php
namespace App;
use Log;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Sentence extends Eloquent 
{
    protected $fillable = ['_id','word','sentence','link','next','prev'];
    protected $collection='Sentence';
    // protected $appends = ['id'];
    // protected $hidden = ['_id'];
    protected $primaryKey='_id';
    public $word,$sDef,$lDef;
}
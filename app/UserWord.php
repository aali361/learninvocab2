<?php
namespace App;
use Log;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class UserWord extends Eloquent 
{
    protected $fillable = ['_id','userId','wordId','learnStatus','lastExample'];
    protected $collection='UserWord';
    protected $primaryKey='_id';
    // protected $hidden = ['_id'];
}
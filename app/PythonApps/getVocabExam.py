#!/usr/bin/python3
import sys
import requests
import json
from bs4 import BeautifulSoup


def insert (source_str, insert_str, pos):
    return source_str[:pos]+insert_str+source_str[pos:]

vocab = sys.argv[1]
mainUrl = "https://www.vocabulary.com/dictionary/"
vocabQuantity=4
url = "https://corpus.vocabulary.com/api/1.0/examples.json?jsonp=jQuery112409688333560257474_1525458749153&query="+vocab+"&maxResults="+str(vocabQuantity)+"&startOffset=0&filter=0&_=1525458749154"
s = requests.session()
r = s.get(url)



result = r.text
# result=result.encode('ascii', 'ignore').decode('ascii')
first = result.find('(')
last = result.rfind(')')


result = result[first+1:last]
result = json.loads(result)

sentences=result['result']['sentences']
for sentence in sentences:
    sentence1 = json.dumps(sentence['sentence'])
    sentence2 = sentence1.lower()
    index = 0
    i = 0
    while index < len(sentence2):
        index = sentence2.find(vocab, index)
        if index == -1:
            break
        sentence1 = insert(sentence1,'<b>',index)
        sentence1 = insert(sentence1,'</b>',index+len(vocab)+3)
        i += 1
        index = index + len(vocab) + 7
        sentence2=sentence1.lower()
    sentence['sentence']=sentence1
    # print(sentence1)


result = json.dumps(result)
print(result)

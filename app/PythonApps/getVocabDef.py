#!/usr/bin/python3
# coding=utf-8
import sys
import json
import requests
from bs4 import BeautifulSoup


vocab = sys.argv[1]
url = "https://www.vocabulary.com/dictionary/"+vocab
s = requests.session()
r = s.get(url)

def insert (source_str, insert_str, pos):
    return source_str[:pos]+insert_str+source_str[pos:]

soup = BeautifulSoup(r.text, 'html.parser')
length = len(soup.findAll("p", {"class": "short"}))
if (length == 0):
    print(0)
else:
    shortDef = soup.findAll("p", {"class": "short"})[0].text
    shortDef = json.dumps(shortDef)
    shortDef2 = shortDef.lower()
    index = 0
    i = 0
    while index < len(shortDef2):
        index = shortDef2.find(vocab, index)
        if index == -1:
            break
        shortDef = insert(shortDef,'<b>',index)
        shortDef = insert(shortDef,'</b>',index+len(vocab)+3)
        i += 1
        index = index + len(vocab) + 7
        shortDef2=shortDef.lower()
    print(shortDef)
    
    longDef = soup.findAll("p", {"class": "long"})[0].text
    longDef = json.dumps(longDef)
    longDef2 = longDef.lower()
    index = 0
    i = 0
    while index < len(longDef2):
        index = longDef2.find(vocab, index)
        if index == -1:
            break
        longDef = insert(longDef,'<b>',index)
        longDef = insert(longDef,'</b>',index+len(vocab)+3)
        i += 1
        index = index + len(vocab) + 7
        longDef2=longDef.lower()
    print(longDef)

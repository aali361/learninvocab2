@extends('master')

@section('title','Vocab Learning Bot Management Panel')

@section('style')
    input{
        font-size:0.8rem !important;   
    }
    .footer{
        font-size:0.7rem !important;
    }
    .btn{
        font-size:0.8rem !important;
    }
    .nilo{
        color:blue;
    }
@endsection

@section('body')
    <div class="content d-flex align-items-center   " style="background-image: url('imgs/login.jpg');background-size:cover;height:100vh;">
        <div class="container">
        <div class="card card-login mx-auto" style="width:400px;margin-top:-50px;">
            <div class="card-header text-right" style="font-size:0.8rem;">
            سامانه مدیریت ربات learnvocab
            </div>

            <div class="card-body">
            <form method="POST" action="login">
                @csrf
                <div class="form-group">
                <div class="form-label-group">
                    <input id="userName" name="userName" class="form-control" placeholder="نام کاربری" autofocus="autofocus">
                    <label class="d-none" for="userName">شماره تلفن همراه</label>
                </div>

                </div>
                <div class="form-group">
                <div class="form-label-group">
                    <input type="password" name="passWord" id="inputPassword" class="form-control" placeholder="رمز عبور">
                    <label class="d-none" for="inputPassword">رمز عبور</label>
                </div>
                </div>
                <button class="btn-primary btn-block btn">ورود</button>
                @if($errors->any())
                <h2 class="pt-3 text-center text-danger" style="font-size:0.8rem;">اطلاعات وارد شده صحیح نیست</h2>
                @endif
            </form>
                <div class="mt-3 footer text-center">طراحی و پیاده سازی شده توسط گروه برنامه نویسی <b class="nilo">نیلوفر</b></div>                
            </div>
        </div>
        </div>
    </div>
@endsection
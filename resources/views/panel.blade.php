@extends("master")

@section("title","پنل مدیریت ربات")

@section('style')
    .content
    {
        <!-- background-image: url('imgs/uni5.jpg'); -->
        <!-- background-size:cover; -->
        <!-- height:100vh; -->
        <!-- direction:rtl; -->
    }
    .nav-item{
        background-color:white;
        font-size:0.8rem;
        <!-- border: 1px solid; -->
    }
    .tab-pane{
        min-height:400px;
        font-size:0.8rem;

    }
    thead{
        font-weight:bold;
    }
    .btn{
        font-size:0.8rem;
        font-family:iransans;
    }
    .inprocess{
        color:green;
        font-weight:bold;
    }
    .modal-body{
        font-family:iransans !important;
    }
    input{
        width:85px !important;
    }
    td{
        text-center !important;
        vertical-align:middle !important;
    }
    tbody
    {
        font-size:0.7rem !important;
    }
    .active-btn .deactive-btn{
        font-size:0.7rem !important;
    }
@endsection

@section('body')
    <div class="content">
        <div class="container">
            <ul class="nav nav-pills pt-5 pr-0" id="myTab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true" onclick="getExamsList()">امتحان</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false" onclick="getUsersList()">کاربران</a>
                </li>
                <!-- <li class="nav-item">
                    <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">تنظیمات بات</a>
                </li> -->
                <li class="nav-item text-left">
                    <a href="/" class="nav-link">خروج</a>
                </li>
            </ul>
            <div class="tab-content text-right" id="myTabContent" style="background-color:white;">
                <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                    <div class="table-responsive p-3">
                        <table  class="table table-bordered">
                            <thead>
                                <tr>
                                    <td>گروه لغات</td>
                                    <td>روز و زمان</td>
                                    <td>نوع سوالات</td>
                                    <td>وضعیت</td>
                                    <td>اقدام</td>
                                </tr>
                            </thead>
                            <tbody id="examsTable">
                            </tbody>
                        </table>
                    </div>
                    <div class="text-left">
                        <button class="btn btn-success m-4" onclick="newExamModal()">ایجاد امتحان جدید</button>
                    </div>
                </div>
                <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                    <div class="table-responsive p-3">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <td>نام</td>
                                    <td>نام خانوادگی</td>
                                    <td>نام کاربری تلگرام</td>
                                    <td>امتیاز</td>
                                    <td>وضعیت</td>
                                    <td>اقدام</td>
                                </tr>
                            </thead>
                            <tbody id="userstable">
                                
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
                    <div class="table-responsive p-3">
                        <table class="table table-bordered text-center">
                            <thead>
                                <tr>
                                    <td>وضعیت ربات</td>
                                    <td>فعال</td>
                                    <td><button class="btn btn-sm btn-danger">غیرفعال کردن</button></td>
                                </tr>
                                <tr>
                                    <td>تعداد کابران</td>
                                    <td>10 نفر</td>
                                </tr>
                                <tr>
                                    <td>تعداد کابران فعال</td>
                                    <td>8 نفر</td>
                                </tr>
                                <tr>
                                    <td>تعداد کاربران غیر فعال</td>
                                    <td>2 نفر</td>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>


     <!-- New Exam Modal -->
    <div class="modal fade" id="newExamModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle"></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            </div>
            <div class="modal-body" style="direction:rtl;">
                <div class="text-right mb-3" style="font-size:0.8rem; font-weight:bold; color:#00b894;">مشخصات امتحان را مشخص کنید:</div>
                <form id="newExam">
                    <table class="table table-bordered table-sm text-center" style="font-family:iransans; direction:rtl;">
                        <thead style="font-size:0.8rem;direction:rtl;">
                            <tr>
                                <td>دسته</td>
                                <td>روز امتحان</td>
                                <td>ساعت امتحان</td>
                                <td>نوع سوالات</td>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="p-3">
                                    <select name="group" >
                                        <option selected value="GRE">GRE</option>
                                        <option value="TOEFL">TOEFL</option>
                                        <option value="IELTH">IELTH</option>
                                    </select>
                                </td>
                                <td><input type="text" style="text-align:center;" name="dayOfExam" id="intervalInputEnd" class="intervalInputEnd" value="2019-05-01"></td>
                                <td><input type="text" style="text-align:center;" name="hourOfExam" id="intervalInputStart" class="intervalInputStart"  value="14:00"></td>
                                <!-- <td><input style="width:50px !important;" type="number" style="text-align:center;" name="timeOfExam" id="intervalInputStart" class="intervalInputStart"  value="20"> دقیقه</td> -->
                                <td class="p-3">
                                    <select name="type" >
                                        <option selected value="WtoS">WtoS</option>
                                        <option value="StoW">StoW</option>
                                    </select>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </form>
            </div>
            <div class="modal-footer" style="direction:ltr;">
                <input type="hidden" value="" id="day">
                <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="closeNewExamModal()">انصراف</button>
                <button type="button" class="btn btn-success" onclick="submitNewExamForm()" >ایجاد</button>
            </div>
            <div class="text-success text-center my-2" id="result" style="font-weight:bold;font-size:0.7rem;font-family:iransans;"></div>

        </div>
        </div>
    </div>

@endsection


@section('scripts')


<script src="/js/persian-date.min.js"></script>


<script>
    function newExamModal()
    {
        var modal=$('#newExamModal');
        $("#result").html("");
        modal.modal('toggle');
    }
    function closeNewExamModal()
    {
        var modal=$('#newExamModal');
        modal.modal('toggle');
    }
    function submitNewExamForm()
    {
        var form=$("#newExam");
        $.ajaxSetup({
            headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        // var examData=form.serializeArray();
        $.ajax({
            type:'POST',
            data: form.serialize(),
            url:'newExam',
            success:function(usersData)
            {
               console.log("Success");
                $("#result").html("امتحان با موفقیت ایجاد شد");
                getExamsList();
            },
            error:function(XMLHttpRequest, textStatus, errorThrown)
            {
                alert("error10545");
                alert("Status: " + textStatus); alert("Error: " + errorThrown); 
            }
        });
    }
    function getUsersList()
    {
        $.ajax({
            type:'POST',
            dataType: 'json',
            url:'/getUsersList',
            success:function(usersData)
            {
                // console.log(usersData);
                var table=$("#userstable");
                table.html("");
                usersData.forEach(function(user)
                {
                    // console.log(user["action"]);
                    table.append("<tr><td>"+user['fname']+"</td><td>"+user['lname']+"</td><td>"+user['username']+"</td><td style='font-weight:bold; direction:ltr;'>"+user['score']+"</td><td style='color:blue;font-weight:bold;'>"+user['disabled']+"</td><td>"+user['action']+"</td></tr>");
                });
            }
        });
    }
    function activeUser(id)
    {
        $.ajaxSetup({
            headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type:'POST',
            // dataType: 'json',
            url:'/activeUser/'+id,
            success:function(usersData)
            {
                console.log("success");
                getUsersList();
            },
            error:function(XMLHttpRequest, textStatus, errorThrown)
            {
                alert("error10545");
                alert("Status: " + textStatus); alert("Error: " + errorThrown); 
            }
        });
    }
    function deActiveUser(id)
    {
        $.ajaxSetup({
            headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type:'POST',
            // dataType: 'json',
            url:'/deActiveUser/'+id,
            success:function(usersData)
            {
                console.log("success");
                getUsersList();

            }
        });
    }

    getExamsList();
    function getExamsList()
    {
        $.ajaxSetup({
            headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type:'GET',
            url:'getExams',
            success:function(examsData)
            {
               console.log("Successfully Getting Exams List");
               var table=$("#examsTable");
                table.html("");
                examsData.forEach(function(exam)
                {
                    // console.log(exam["action"]);
                    table.append("<tr><td>"+exam['group']+"</td><td>"+exam['time']+"</td><td>"+exam['type']+"</td><td>"+exam['status']+"</td><td>"+exam['action']+"</td></tr>");
                });
            },
            error:function(XMLHttpRequest, textStatus, errorThrown)
            {
                alert("error10545");
                alert("Status: " + textStatus); alert("Error: " + errorThrown); 
            }
        });
    }

    function examSuspend(id)
    {
        console.log(id);
        $.ajaxSetup({
            headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type:'POST',
            data: {'id':id},
            url:'suspendExam',
            success:function(usersData)
            {
                console.log("success");
                getExamsList();

            }
        });
    }

</script>



@endsection
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>@yield('title')</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    @yield('link')
    <style>
        @font-face
        {
            font-family: iransans;
            font-style: normal;
            font-weight: normal;
            font-display: swap;
            src: url(/fonts/IRANSans-web.woff2);
        }
        @font-face
        {
            font-family: iransans;
            font-style: normal;
            font-weight: lighter;
            font-display: swap;
            src: url(/fonts/IRANSans-Light-web.woff2);
        }
        @font-face
        {
            font-family: iransans;
            font-style: normal;
            font-weight: bold;
            font-display: swap;
            src: url(/fonts/IRANSans-Bold-web.woff2);
        }
        .content{
            font-family: iransans;
            direction:rtl;
        }
        @yield('style')
    </style>
</head>
<body>
    @yield('body')
    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
    <script src="/js/persian-date.min.js"></script>
    @yield('scripts')
</body>
</html>